import { MigrationInterface, QueryRunner } from 'typeorm'

export class InitDatabase1562492132259 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "message" ("id" SERIAL NOT NULL, "message" text NOT NULL, CONSTRAINT "PK_ba01f0a3e0123651915008bc578" PRIMARY KEY ("id"))`
    )
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "message"`)
  }
}
